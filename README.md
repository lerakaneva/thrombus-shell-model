# Model of thrombus shell formation

This repository presents the model described in the following paper:
https://www.cell.com/biophysj/fulltext/S0006-3495(20)33213-6

Don't hesitate to contact lerakaneva@gmail.com if you have any problems with the code.

## Before you start
No one has ever tried to run it on Windows, only Linux or MacOs.

### Install boost library
First you will need to install boost library.
example for mac os:
* brew install boost

Then you'll have to find the boost root and library location
In my case  (for macOs + install via with brew) it was:
* /usr/local/Cellar/boost/1.84.0_1
* /usr/local/Cellar/boost/1.84.0_1/lib

### Clone repository and add correct boost location for your local version
Press "Code" on the home repository page to find clone instructions
Locally modify run_cmake.sh - replace DBOOST_ROOT:PATHNAME and DBoost_LIBRARIES:FILEPATH
with your local path if it differs

cmake -DBOOST_ROOT:PATHNAME=/usr/local/Cellar/boost/1.84.0_1 \
    -DBoost_LIBRARIES:FILEPATH=/usr/local/Cellar/boost/1.84.0_1/lib ..

## How to build
* bash run_cmake.sh

## How to run
We'll run in from the directory that contains thrombus-shell-model folder
So if you are in thrombus-shell-model right now, do:
* cd ..

How to run 20s calculation:
* ./trombo --end_time 20000
end_time is the calculation time in millisecond

You can also find all saved dumps in data/dumps
if you copy one of the dumps:
* cp data/dumps/partsystem_100.txt partsystem.txt

you'll be able to continue calculation (until the time reached end_time milliseconds, in this example, 40000)
* ./trombo --end_time 40000 --continue


## How to visualize:
Go to the directory:
* cd thrombus-shell-model/visual

And run python script:
* python visual.py --directory ../../data

data/coordinates_ParticleVis.txt is the file with the platelet coordinates and activation states




-----
Who contributed to this version of code:
Valeria Kaneva
Peter Trifanov
Vlad Kuznetsov

