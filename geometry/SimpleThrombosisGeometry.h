#pragma once

#include "geometry.h"

class SimpleThrombosisGeometry : virtual public Geometry {
public:
    static const size_t n_anchored;

    SimpleThrombosisGeometry()
        : Geometry(Thrombosis) {
        border_points = 
            {{{0, 0}, {150, 0}, {150, 35}, {0, 35}, {0, 0}}};  
    }

    virtual void placeAnchPart(std::vector<Particle*>& anch_part, unsigned int &next_number) {
        double x = (border_points[1][0] - border_points[0][0]) / 4,
            y = -1, phi_z = 0;
        for (unsigned int i = 0; i < anch_part.size(); i++) {
            x += 2 * Particle::particleR / Particle::redFactorAnchor;
            //Create new initially adhered platelet:
            anch_part[i]->x = x;
            anch_part[i]->y = y;
            anch_part[i]->z = 0;
            anch_part[i]->x_last = x;
            anch_part[i]->y_last = y;
            anch_part[i]->z_last = 0;
            anch_part[i]->phi_z = phi_z;
            anch_part[i]->phi_z_last = 0;
            anch_part[i]->ident = next_number;
            anch_part[i]->ext_activation = (next_number % Particle::redFactorAnchor == 0) * 1.0;
            anch_part[i]->time_activation = Particle::tau_act * 100.0;
            anch_part[i]->state = next_number % Particle::redFactorAnchor == 0 ? isSeed : isDummy;
            // Increase the value that is in charge of numbering of particles:
            next_number++;
        }
    }

    virtual std::array<double, 3> getNewParticleCoords() {
        double x = 0, y = 0, z = 0;
        y = Random::urand_num(0, 1) * border_points[3][1];
        return {x, y, z};
    }

    virtual void setWallForce(Particle &p) {
        if (p.y < p.selfR)  // Collision with lower wall.
            p.f_y += p.k_wall * (p.selfR - p.y);
        if (p.y > border_points[3][1] - p.selfR) // Collision with upper wall.
            p.f_y += p.k_wall * (border_points[3][1] - p.selfR - p.y);
    }
    
    virtual bool isOut(Particle &p) {
        return p.x < border_points[0][0] 
            || p.x > border_points[1][0];
    }

    virtual void step(long) {};
};
