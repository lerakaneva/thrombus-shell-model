#pragma once

#include <vector>
#include <array>
#include "../geometry/geometry.h"

struct Particle;

class Hydrodynamics {
public:
    // constants (have to be properly manually set in OpenFOAM files!)
    static const double flow_max_vel;
    static const double viscos;
    static const double viscos_w;
    static const double rho;              // density
    static const double etha;

    Geometry* geo; // in case hydro class will need it, i.e. for mesh updating

/*    
    static Hydrodynamics& Instance()
    {
        static Hydrodynamics hydro();
        return hydro;
    }
*/
    Hydrodynamics() {};
    virtual void initialize(const std::vector<Particle*>&) = 0;
    virtual void step(unsigned long long, const std::vector<Particle*>&) = 0;
    virtual void setViscousForce(Particle*) = 0;
    virtual std::array<double, 3> operator()(double x, double y) = 0;

private:
    Hydrodynamics(Hydrodynamics&) = delete;  // to forbid copying
    Hydrodynamics(Hydrodynamics&&) = delete; // to forbid moving
};