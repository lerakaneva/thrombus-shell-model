#pragma once

#include "../main.h"
#include "hydrodynamics.h"
#include <map>
#include <deque>

class SimpleHydrodynamics : public Hydrodynamics {
public:
    void markThrombus(const std::vector<Particle*>&);
    void step(unsigned long long, const vector<Particle*>&);
    virtual void initialize(const vector<Particle*>&);
    // operator() returns velocity
    std::array<T,3> operator()(T, T y) {
        T rel_y = y/Consts::cavity_size[1];
        return {flow_max_vel * 4 * rel_y * (1 - rel_y), 0, 0};
    }

    void setViscousForce(Particle* plt) { 
        std::array<double, 3> U = (*this)(plt->x, plt->y);
        if (plt->state == onBorder || plt->state == isFree){
		    plt->f_x += Hydrodynamics::viscos * U[0];
		    plt->f_y += Hydrodynamics::viscos * U[1];
        }
    }
};

