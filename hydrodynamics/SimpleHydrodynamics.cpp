#include "SimpleHydrodynamics.h"
#include <set>

void SimpleHydrodynamics::markThrombus(const std::vector<Particle*>& pv)
{
	// mark free platelets
	for(Particle* p : pv)
		if( p->state != isSeed && p->state != isDummy )
			p->state = isFree;

	// calculate pairwise distances and create graph
	std::map<int, std::vector<Particle*>> graph;
	for(size_t i = 0; i < pv.size(); ++i)
		for(size_t j = i + 1; j < pv.size(); ++j)
			if( pv[i]->distance(*(pv[j])) < 1.05 * (pv[i]->selfR + pv[j]->selfR) )
			{
				graph[pv[i]->ident].push_back(pv[j]);
				graph[pv[j]->ident].push_back(pv[i]);
			}

	// BFS
	static std::deque<unsigned int> queue;
	queue.clear();
	for(Particle* p : pv)
		if( p->state == isSeed || p->state == isDummy )
			queue.push_back(p->ident);
	while(queue.size() > 0)
	{
		for(Particle* p : graph[queue.front()])
			if( p->state == isFree )
			{
				queue.push_back(p->ident);
				p->state = inThrombus;
			}
		queue.pop_front();
	}

	// find platelets on the boundary
	double xt_max = -1;
	double xt_min = -1;
	double yt_max = -1;
	for(Particle* p : pv)
	    if (p->state == inThrombus || p->state == isSeed || p->state == isDummy){
			if ((xt_max < 0) or (p->x > xt_max))
			    xt_max = p->x;
			if ((xt_min < 0) or (p->x < xt_min))
			    xt_min = p->x;
			if ((yt_max < 0) or (p->y > yt_max))
			    yt_max = p->y;				
		}

	if (xt_max < 0)
	    return;
	xt_max = 0.25 * ceil(4 * (xt_max + 1));
	xt_min = 0.25 * floor(4 * (xt_min - 1));
	yt_max = 0.25 * ceil(4 * (yt_max + 1));
	
	for (double xt_cur = xt_min; xt_cur <= xt_max; xt_cur += 0.25){
		Particle* pltAddUp = nullptr;
		double ytt_max = 0;
		for(Particle* p : pv){
		    if ((p->state == inThrombus || p->state == isSeed || p->state == isDummy || p->state == onBorder) && (fabs(p->x - xt_cur) < 1.0)){
				if ((!pltAddUp) || (p->y + sqrt(1 - (xt_cur - p->x) * (xt_cur - p->x)) > ytt_max)){
					pltAddUp = p;
					ytt_max = p->y + sqrt(1 - (xt_cur - p->x) * (xt_cur - p->x));
			    }
			}
		}
		if (pltAddUp){
			if (pltAddUp->state != isSeed && pltAddUp->state != isDummy){
		        pltAddUp->state = onBorder;
			}
		}
	}

	for (double yt_cur = 0; yt_cur <= yt_max; yt_cur += 0.25){
		Particle* pltAddLeft = nullptr;
		Particle* pltAddRight = nullptr;
		double xtt_max = 0;
		double xtt_min = 0;
		for(Particle* p : pv)
		    if ((p->state == inThrombus || p->state == isSeed || p->state == isDummy || p->state == onBorder) && (fabs(p->y - yt_cur) < 1.0)){
				if ((!pltAddLeft) || (p->x + sqrt(1 - (yt_cur - p->y) * (yt_cur - p->y)) < xtt_min)){
					pltAddLeft = p;
					xtt_min = p->x + sqrt(1 - (yt_cur - p->y) * (yt_cur - p->y));
			    }
				if ((!pltAddRight) || (p->x + sqrt(1 - (yt_cur - p->y) * (yt_cur - p->y)) > xtt_max)){
					pltAddRight = p; 
					xtt_max = p->x + sqrt(1 - (yt_cur - p->y) * (yt_cur - p->y));
			    }
			}
		if (pltAddRight){
			if (pltAddRight->state != isSeed && pltAddRight->state != isDummy){
		        pltAddRight->state = onBorder;
			}
		}
		if (pltAddLeft){
			if (pltAddLeft->state != isSeed && pltAddLeft->state != isDummy){
		        pltAddLeft->state = onBorder;
			}
		}
	}
}

void SimpleHydrodynamics::initialize(const vector<Particle*>& pv) {
    markThrombus(pv);
};

void SimpleHydrodynamics::step(unsigned long long st, const vector<Particle*>& pv) {
	if (st % 100000 == 0)
        markThrombus(pv);
};

