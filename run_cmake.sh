#!/bin/bash
[ -d build ] || mkdir build
cd build

cmake -DBOOST_ROOT:PATHNAME=/usr/local/Cellar/boost/1.84.0_1 \
    -DBoost_LIBRARIES:FILEPATH=/usr/local/Cellar/boost/1.84.0_1/lib ..

cmake --build . -j 3
mv trombo ../..