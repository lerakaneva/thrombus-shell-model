#pragma once

#include "main.h"
#include "particle/particle.h"

template<typename MP, typename AP, typename H, typename G>
class ParticlesContainer {
    static const double new_part_prob;  // Frequency of new particles generation.
    vector<MP> mov_part;
    vector<AP> anch_part;
    H& flow;
    G& geo;
    unsigned int next_number = 0;  // variable that number particles in the system
    ParticlesContainer(ParticlesContainer&) = delete; // forbid copying
    int time_step = 0;
    // File for platelets coordinates:
    std::ofstream out;
    std::ofstream out_color;
    std::ofstream out_forces;
    std::ofstream out_props;  // for additional properties: state, size
    // buffers for file IO
    const int buf_size = 2048000; // 2M
    char *buf_out, *buf_out_color, *buf_out_forces, *buf_out_props;

  public:
    int writeCSVFreq;  // Frequency (number of steps) of writing simulation data to a file.
    int writeStateFreq; // for system state data

    ParticlesContainer(H& flow, G& geo, bool begin = true)
        : flow(flow), geo(geo) {
        mov_part.reserve(1 << 20);
        anch_part.reserve(1 << 10);
        flow.geo = &geo;
        geo.writeDescFile();
        std::ios_base::openmode mode = begin ? std::ios_base::out : std::ios_base::app;
        // create IO buffers of specified size for fstreams
        char *buf_out = new char[buf_size];
        char *buf_out_color = new char[buf_size];
        char *buf_out_forces = new char[buf_size];
        char *buf_out_props = new char[buf_size];
        out.rdbuf()->pubsetbuf(buf_out, buf_size);
        out_color.rdbuf()->pubsetbuf(buf_out_color, buf_size);
        out_forces.rdbuf()->pubsetbuf(buf_out_forces, buf_size);
        out_props.rdbuf()->pubsetbuf(buf_out_props, buf_size);
        out.open("data/coordinates_ParticleVis.txt", mode);
        out_color.open("data/colormap_ParticleVis.txt", mode);
        out_forces.open("data/forces.txt", mode);
        out_props.open("data/props.txt", mode);

        if( begin ) {
            for(unsigned int i = 0; i < geo.n_anchored; ++i)
                anch_part.emplace_back();
            std::vector<Particle*> tmp_anch;
            for(unsigned int i = 0; i < geo.n_anchored; ++i)
                tmp_anch.push_back(&anch_part[i]);
            geo.placeAnchPart(tmp_anch, next_number);
        } else {
            loadState();
        }
    }

    ~ParticlesContainer() {
        // no need to delete buffers here, because streams handle it themselves
        out.close();
        out_color.close();
        out_forces.close();
        out_props.close();
    }

    size_t size() {
        return mov_part.size() + anch_part.size();
    }

    // Output
    void writeCSV(long iter) {
        int help = 0;
        out << "* " << iter << " \n";
        for (Particle& m_p : mov_part){
            out << " " << m_p.x << " " << m_p.y << " " << 0.0 << " \n";
            out_color << help << " " << iter << " " << 255 << " "<<  255 - int(255 *  m_p.ext_activation) << " "
                << (m_p.time_activation < Particle::tau_act ? 255 : 0) << " \n";
            out_props << " " << m_p.state % NUM_STATES << " " << m_p.selfR << " ";
            help++;
        }
        for (Particle& a_p : anch_part) {
            out << " " << a_p.x << " " << a_p.y << " " << 0.0 << " \n";
            out_color << help << " " << iter << " " << (a_p.state != isDummy ? 0 : 255) << " "
                <<  255 << " " << 255 << " \n";
            out_props << " " << a_p.state % NUM_STATES << " " << a_p.selfR << " ";
            help++;
        }
        out_props << "\n";

        // This is necessary procedure for ParticleVis program
        // which can not work with variable number of particles.
        // We add imaginary particles and thus fix the number (100).
        for (int h = mov_part.size() + anch_part.size(); h < 500; h++) {
            out << " " << 0.0 << " " << 0.0 << " " << 0.0 << " \n";
            out_color << h << " " << iter << " " << 0 << " " <<  0 << " " << 0 << " \n";
        }

        for(Particle& m_p : mov_part)
            out_forces << " " << m_p.hyd_x << " " << m_p.hyd_y << " " << m_p.state % NUM_STATES << " ";
        for(Particle& a_p : anch_part)
            out_forces << " " << a_p.hyd_x << " " << a_p.hyd_y << " " << a_p.state % NUM_STATES << " ";
        out_forces << "\n";
    }
    
    void flushAll() {
        out.flush();
        out_color.flush();
        out_props.flush();
        out_forces.flush();
    }

    /**
    * this function is used to create a full dump of current state of particle system
    * to allow to reload it and continue computations
    */
    void dumpState(long iter) {
        std::ofstream dump;
        char *buf = new char[204800]; // 200K should be probably enough to write all file at once
        dump.rdbuf()->pubsetbuf(buf, 204800);
        dump.open("data/dumps/partsystem_" + std::to_string(iter) + ".txt", std::ios_base::out);
        dump << "time:" << iter << "\n"
            << "size:" << mov_part.size() + anch_part.size() << "\n";
        for(Particle& a_p : anch_part) {
            dump << "type:anchored\n";
            dump << a_p;
        }
        for(Particle& m_p : mov_part) {
            dump << "type:moving\n";
            dump << m_p;
        }
        dump << std::endl;
        dump.close();
    }
    /**
    * this function is used to loading state file
    */
    void loadState() {
        std::ifstream load("partsystem.txt", std::ios_base::in);
        if( !load.good() ) {
            std::cout << "Problems with opening statefile partsystem.txt\n";
            exit(-1);
        }
        std::string line;
        while( std::getline(load, line) )  // note: getline doesn't read \n at the end
        {
            if( line.size() == 0 ) // skip empty lines
                continue;
            size_t delim = line.find(':');
            std::string key = line.substr(0, delim);
            std::string value = line.substr(delim + 1, line.size() - delim - 1);
            if( key == "time" ) {
                    time_step = std::stol(value);
                    std::cout << "Loading time step " << time_step << "\n";
            } else if( key == "size" ) {
                    size_t size = stoul(value);
                    std::cout << "File contains " << size << " particles\n";
            } else if( key == "type" ) {
                if( value == "moving" ) {
                    mov_part.resize(mov_part.size() + 1);
                    (mov_part.back()).loadParticle(load);
                } else if( value == "anchored") {
                    anch_part.resize(anch_part.size() + 1);
                    (anch_part.back()).loadParticle(load);
                } else {
                    std::cerr << "Unknown particle type " << value << "\n";
                }
            } else {
                std::cerr << "Unknown field " << key << " with value " << value << "\n";
            }
        }
        // find maximum ident among all particles and use its value + 1 as new next_number
        next_number = std::max_element(anch_part.begin(), anch_part.end(),
            [](Particle& p1, Particle& p2){return p1.ident < p2.ident;})->ident;
        next_number = std::max(next_number, std::max_element(mov_part.begin(), mov_part.end(),
            [](Particle& p1, Particle& p2){return p1.ident < p2.ident;})->ident);
        ++next_number;
        std::cout << "next_number set to " << next_number << "\n";
        std::cout << "Finished loading\n";
    }

    unsigned long getInitialStep() { return time_step; }

    void gen_new_particle()
    {
		if (Random::big_urand_num(0, 1) < new_part_prob) {
            mov_part.emplace_back(geo.getNewParticleCoords(), flow, next_number);
            next_number++;
        }
    }

    ParticlesContainer &operator++() { // Dynamics of particles.
        gen_new_particle();
        unsigned int len = mov_part.size();

        for (unsigned int i = 0; i < len; i++) {
            for (unsigned int j = i + 1; j < len; j++)
                mov_part[i].interact_with(mov_part[j]);
            for (AP& a_p : anch_part)
                mov_part[i].interact_with(a_p);
        }

        for(MP &m_p : mov_part)
        {
            geo.setWallForce(m_p);
            m_p.interact_with(flow);
            m_p.updateDynamics();
            m_p.updateActivation();
        }

        for (unsigned int i = 0; i < len; i++) {
            if(geo.isOut(mov_part[i])) { // Particle is out of calculating area.
                // Break all the bonds with "i" particle:
                for (unsigned int j = 0; j < len; j++)
                    mov_part[i].kill_bond_one_side(mov_part[j]);
                for (AP& a_p : anch_part)
                    mov_part[i].kill_bond_one_side(a_p);
            }
        }

        // Create a vector of particles that are still in calculating area:
        auto new_end = std::remove_if(mov_part.begin(), mov_part.end(),
                [this](MP& m_p) {
                    return geo.isOut(m_p);
                });

         // Delete particles out of calculating area:
        mov_part.erase(new_end, mov_part.end());

        return *this;
    }

    Particle& operator[](int i) {
        if( i < (int)anch_part.size() )
            return anch_part[i];
        else
            return mov_part[i - anch_part.size()];
    }

    Particle& id(int i) {
        for(int k = 0; k < size(); ++k)
            if( (*this)[k].ident == i )
                return (*this)[k];
    }
    
    int movThromsusSize() const {
        return std::count_if(mov_part.begin(), mov_part.end(),
            [](const MP &m_p){return m_p.state == inThrombus;});
    }

    /**
    * this function can return vector of all particles
    * to use them in hydro class
    */
    std::vector<Particle*> particles() {
        std::vector<Particle*> res((*this).size());
        for(size_t i = 0; i < anch_part.size(); ++i)
            res[i] = &(anch_part[i]);
        for(size_t i = 0; i < mov_part.size(); ++i)
            res[anch_part.size() + i] = &(mov_part[i]);
        return res;
    }
};
