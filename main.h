#pragma once

typedef double T;
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
#include <fstream>
using std::ofstream;
#include <cmath>
using std::sqrt;
#include <array>
#include <algorithm>

#include "random/RandomRand.h"
typedef RandomRand Random;

struct Consts {
    static const double pi;
    static double const dt;
    static double const factor1;
    static double const factor2;
    static double const factor1w;
    static double const factor2w;
    static double const cavity_size[2];
};

