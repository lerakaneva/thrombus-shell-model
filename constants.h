#pragma once

const double Consts::pi = 3.14159265358979323846;

//max X and max Y of the area of the vessel:
const double Consts::cavity_size[2] = {150, 35};
const double Consts::dt = 1e-4; //  Time step.

const double Particle::particleR = 1;
const double Particle::particleMass = 4;
const double Particle::tau_act = 1200 * 1 / Consts::dt;  // Half-time of activation.
const unsigned int Particle::redFactorAnchor = 4;
const double Particle::levelMinActiv = 0.6;
const double Particle::delta = 1.0 / sqrt(1000.0); //1.0 / sqrt(100000000.0);

// Geometry and flow
const size_t SimpleThrombosisGeometry::n_anchored = 89; // lenght of the injury

// Velocity in the center of the vessel:
const double Hydrodynamics::flow_max_vel = 35.0 / 4.0;
// Viscosity:
const double Hydrodynamics::viscos = 3141. * 6;
const double Hydrodynamics::viscos_w = 314. * 6;
const double Hydrodynamics::rho = 1;
const double Hydrodynamics::etha = Hydrodynamics::viscos / 6 / Consts::pi / Particle::particleR;

// Factors for Verlet scheme:
const double Consts::factor1 = 1 / (1 + Consts::dt * Hydrodynamics::viscos /
                                     (2 * Particle::particleMass));
const double Consts::factor2 = (1 - Consts::dt * Hydrodynamics::viscos /
                                 (2 * Particle::particleMass));
const double Consts::factor1w = 1 / (1 + Consts::dt * Hydrodynamics::viscos_w /
    (Particle::particleMass * Particle::particleR * Particle::particleR));
const double Consts::factor2w = (1 - Consts::dt * Hydrodynamics::viscos_w /
    (Particle::particleMass * Particle::particleR * Particle::particleR));

// Particle generation
//template<typename MP, typename AP, typename H, template<typename g_MP, typename g_AP> class G>
template<typename MP, typename AP, typename H, typename G>
const double ParticlesContainer<MP, AP, H, G>::new_part_prob = 1.0 / 9.0 * Consts::dt; //1e-5 for dt=1e-4ms

// Constants for pl-pl interaction via integrins (Morse)
const double MovingMorseParticle::alpha = 20;
  // Corresponds to 0.01 of force between 2 fully activated platelets in our model:
const double MovingMorseParticle::k_morse = 10000000;//10000000;
// Constants for pl-pl interaction via GPIb (stochastic springs)
    // Association:
const double MovingMorseParticle::k_as_0 = 9;
const double MovingMorseParticle::alpha_as = - 10;
    // General
const double MovingMorseParticle::eq_spring_l = 0.025;
const double MovingMorseParticle::k_stochastic = 30000;
    // Dissociation:
const double MovingMorseParticle::k_dis_0 = 0.0171;
const double MovingMorseParticle::bell_dis = 0.000001358;
// Elastic interaction:
const double Particle::k_wall = 1500000;
const double MovingMorseParticle::k_repulsion = 500000;//105e5;
