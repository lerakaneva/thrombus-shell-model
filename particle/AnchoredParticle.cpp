#include "AnchoredParticle.h"

void AnchoredParticle::doPrint(std::ostream& os) const {
    // output all data which is needed to load, e.g. forces will be recalculated
    // so no need to store them
    os << "ident:" << ident << "\n"
        << "x:" << x << "\n"
        << "y:" << y << "\n"
        << "z:" << z << "\n"
        << "x_last:" << x_last << "\n"
        << "y_last:" << y_last << "\n"
        << "z_last:" << z_last << "\n"
        << "phi_z:" << phi_z << "\n"
        << "phi_z_last:" << phi_z_last << "\n"
        << "v_x:" << v_x << "\n"
        << "v_y:" << v_y << "\n"
        << "v_z:" << v_z << "\n"
        << "w_z:" << w_z << "\n";
    os << "bonds:";
    for(size_t i = 0; i < bonds.size(); ++i) {
        os << bonds[i];
        if( i != bonds.size() - 1 )
            os << ',';
    }
    os << "\n";
    os << "bonds_angles:";
    for(size_t i = 0; i < bonds_angles.size(); ++i) {
        os << bonds_angles[i];
        if( i != bonds.size() - 1 )
            os << ',';
    }
    os << "\n";
    os << "ext_activation:" << ext_activation << "\n"
        << "time_activation:" << time_activation << "\n"
        << "selfR:" << selfR << "\n"
        << "state:" << state % NUM_STATES << "\n"
        << "onceAttached:" << onceAttached << "\n";
    os << "end\n";
}

void AnchoredParticle::loadParticle(std::ifstream& dump) {
    std::string line;
    while(std::getline(dump, line), line != "end") {
        size_t delim = line.find(':');
        std::string key = line.substr(0, delim);
        std::string value = line.substr(delim + 1, line.size() - delim - 1);
        if( key == "ident") {
            ident = std::stoll(value);
        } else if( key == "x") {
            x = std::stod(value);
        } else if( key == "y") {
            y = std::stod(value);
        } else if( key == "z") {
            z = std::stod(value);
        } else if( key == "x_last") {
            x_last = std::stod(value);
        } else if( key == "y_last") {
            y_last = std::stod(value);
        } else if( key == "z_last") {
            z_last = std::stod(value);
        } else if( key == "phi_z") {
            phi_z = std::stod(value);
        } else if( key == "phi_z_last") {
            phi_z_last = std::stod(value);
        } else if( key == "v_x") {
            v_x = std::stod(value);
        } else if( key == "v_y") {
            v_y = std::stod(value);
        } else if( key == "v_z") {
            v_z = std::stod(value);
        } else if( key == "w_z") {
            w_z = std::stod(value);
        } else if( key == "ext_activation") {
            ext_activation = std::stod(value);
        } else if( key == "time_activation") {
            time_activation = std::stod(value);
        } else if( key == "selfR") {
            selfR = std::stod(value);
        } else if( key == "state") {
            state = (PlateletState)std::stoi(value);
        } else if( key == "onceAttached") {
            onceAttached = (bool)std::stoi(value);
        } else if( key == "bonds") {
            size_t comma;
            while( comma = value.find(','), comma != std::string::npos ) {
                bonds.push_back(std::stoi(value.substr(0, comma)));
                value = value.substr(comma + 1, value.size() - comma - 1);
            }
            if( value.size() > 0 ) {  // there is no comma after last number
                bonds.push_back(std::stoi(value));
            }            
        } else if( key == "bonds_angles") {
            size_t comma;
            while( comma = value.find(','), comma != std::string::npos ) {
                bonds_angles.push_back(std::stod(value.substr(0, comma)));
                value = value.substr(comma + 1, value.size() - comma - 1);
            }
            if( value.size() > 0 ) {  // there is no comma after last number
                bonds_angles.push_back(std::stod(value));
            }            
        } else {
            std::cerr << "Unknown field " << key << " with value " << value
                << " for particle of type anchored\n";
        }
    }
}