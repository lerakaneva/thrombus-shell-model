#include "movingMorseParticle.h"

std::default_random_engine MovingMorseParticle::gen(time(NULL));
std::normal_distribution<double> MovingMorseParticle::dist(0, 1);

MovingMorseParticle::MovingMorseParticle(T x, T y, T z, unsigned int ident,
    float ext_activation, int time_activation, PlateletState state) :
    Particle(x, y, z, x, y, z, 0, 0, ident, ext_activation, time_activation, state)
    {
        v_x = v_y = v_z = 0;
        w_z = 0;
        f_x = f_y = f_z = m_z = 0;
    }

MovingMorseParticle::MovingMorseParticle(std::array<double, 3> coords, Hydrodynamics& flow, unsigned int ident) {
    // Initial conditions for particle:
    x = coords[0], y = coords[1], z = coords[2];phi_z = 0;
    std::array<T,3> vel = flow(x, y);
    v_x = vel[0];
    v_y = vel[1];
    v_z = vel[2];
    w_z = 0;
    x_last = x - v_x * Consts::dt;
    y_last = y - v_y * Consts::dt;
    z_last = z - v_z * Consts::dt;
    phi_z_last = phi_z - w_z * Consts::dt;
    f_x = f_y = f_z = m_z = 0;
    ext_activation = 0;
    time_activation = 0;
    state = isFree;
    this->ident = ident;

    selfR = particleR;

    bonds.reserve(20);  // Just in case.
    bonds_angles.reserve(20);
}

void MovingMorseParticle::interact_with(Particle& another) {
    T l = this->distance(another);
    if (l == 0){
        std::cout << "l=0 \n";
    } else {
        T f = 0;
        T f_x = 0;
        T f_y = 0;
        T f_z = 0;
        T m_z_this = 0;
        T m_z_another = 0;
        // Stochastic interaction (GPIb + vWF) [springs]:
        if( !this->bond_exists(another) ) { // Bond have not been created.
            if( l < 1.5 * (selfR + another.selfR) ) {  // Otherwise bond is unlikely to be created.
                double as_rate = 0; // Probability to create a bond.
                double as_rate_rand = Random::big_urand_num(0, 1); // Auxiliary variable.
                if( l < selfR + another.selfR + 2 * eq_spring_l )
                    as_rate = k_as_0 * Consts::dt;
                else
                    as_rate = k_as_0 * Consts::dt *
                        exp(alpha_as * pow(l - selfR - another.selfR - 2 * eq_spring_l, 2));

                // Taking into account extent of activation:
                double beta_i = 0;
                double  beta_j = 0;
                if( this->ext_activation < Particle::levelMinActiv )
					beta_i = this->ext_activation / Particle::levelMinActiv;
                else
                    beta_i = 1;

                if( another.ext_activation < Particle::levelMinActiv )
					beta_j = another.ext_activation / Particle::levelMinActiv;
                else
                    beta_j = 1;

                beta_i *= (1 - Particle::delta);
                beta_j *= (1 - Particle::delta);

                beta_i += Particle::delta;
                beta_j += Particle::delta;

                as_rate *= (beta_i * beta_j);

                // We describe stochastic process this way:
                // we calculated probability and generated random number [0,1]
                // the probability that random number will be in range
                // (0, a), 0 >= a >= 1 is proportional to a;
                // that is the reason why we apply this condition:
                if( as_rate_rand < as_rate ) {
                    this->create_bond(another);
                    if( (this->time_activation > 0) && (another.time_activation == 0) )
                        another.time_activation = 1;
                    else if( (another.time_activation > 0) && (this->time_activation == 0) )
                        this->time_activation = 1;
                }
            }
        } else { // If bond exists.
            // Angles of bond (spring) direction:
            double angle_this = this->return_angle(another);
            double angle_another = another.return_angle(*this);
            // Coordinates of bonds(springs) ends:
            double x_spr_this = this->x + selfR * cos(angle_this);
            double x_spr_another = another.x + another.selfR * cos(angle_another);
            double y_spr_this = this->y + selfR * sin(angle_this);
            double y_spr_another = another.y + another.selfR * sin(angle_another);
            // Length of spring which ends are fixed on platelets surfaces:
            T ll = sqrt(pow(x_spr_this - x_spr_another, 2) +
                pow(y_spr_this - y_spr_another, 2));
            // Calculate force and torque:
            f = - k_stochastic * (ll - 2 * eq_spring_l);

            if((ll - 2 * eq_spring_l) * ((x_spr_this - x_spr_another) *
	            		(this->x - another.x) + (y_spr_this - y_spr_another) * (this->y - another.y)) > 0)
                {
                f_x = f * (x_spr_this - x_spr_another) / ll;
                f_y = f * (y_spr_this - y_spr_another) / ll;
                f_z = 0;
            } else {
                f_x = 0;
                f_y = 0;
                f_z = 0;
            }
            // Torque is defined as the cross product of the vector
            // by which the force's application point is offset relative
            // to the fixed suspension point (distance vector)
            // and the force vector, which tends to produce rotational motion.
            // (wikipedia.org)
            m_z_this = (x_spr_this - this->x) * f_y -
                (y_spr_this - this->y) * f_x;
            m_z_another = -(x_spr_another - another.x) * f_y +
                (y_spr_another - another.y) * f_x;

            // Calculate probability of bond dissociation:
            double dis_rate = 0;  // Probability of bond dissociation.
            double dis_rate_rand = Random::big_urand_num(0, 1); // Auxiliary variable.
            if(( ll - 2 *(eq_spring_l) < 0 ) || (l < selfR + another.selfR))
                dis_rate = k_dis_0 * Consts::dt;
            else
                dis_rate = k_dis_0 * Consts::dt * exp(fabs(f) * bell_dis);

            // We describe stochastic process this way:
            // we calculated probability and generated random number [0,1]
            // the probability that random number will be in range
            // (0, a), 0 >= a >= 1 is proportional to a;
            // that is the reason why we apply this condition:
            if( dis_rate_rand < dis_rate ) {
                this->kill_bond(another);
            }
        }

        // Here we need to take into account, that we have dummy particles anchored:
        if( another.state != isDummy ) {
            // Deterministic interaction (Integrins + fg):
            if (l < this->selfR + another.selfR + Particle::particleR
                    && l >= this->selfR + another.selfR) {
                T tmp_exp = exp(-alpha * (l - this->selfR - another.selfR));
                f = k_morse * this->ext_activation * another.ext_activation * (1 - tmp_exp) * tmp_exp;
            } else if(l < this->selfR + another.selfR) {
                f = k_repulsion * (l - this->selfR - another.selfR);
            } else {
                f = 0;
            }

        //    // Here we need to take into account, that we have dummy particles anchored:
        //    if( (another.ident < 60) && (another.ident % 4 != 0) )  // 60 - number of anchored particles...
        //        f = 0;

            f_x += f * (another.x - this->x) / l;
            f_y += f * (another.y - this->y) / l;
            f_z += f * (another.z - this->z) / l;
        }

        this->updateForces(f_x, f_y, f_z, m_z_this);
        another.updateForces(-f_x, -f_y, -f_z, m_z_another);
    }
}

void MovingMorseParticle::updateForces(T f_x, T f_y, T f_z, T m_z) {
    this->f_x += f_x;
    this->f_y += f_y;
    this->f_z += f_z;
    this->m_z += m_z;
}

void MovingMorseParticle::interact_with(Hydrodynamics& flow) {
    flow.setViscousForce(this);
}

void MovingMorseParticle::updateDynamics() {
    T x_help = x;
    T y_help = y;
    T z_help = z;
    T phi_z_help = phi_z;

    // Verlet scheme, update coordinates
        x = Consts::factor1 * (2 * x - Consts::factor2 * x_last +
                        f_x * Consts::dt * Consts::dt / Particle::particleMass);
        y = Consts::factor1 * (2 * y - Consts::factor2 * y_last +
                        f_y * Consts::dt * Consts::dt / Particle::particleMass);
        z = Consts::factor1 * (2 * z - Consts::factor2 * z_last +
                        f_z * Consts::dt * Consts::dt / Particle::particleMass);
        phi_z = Consts::factor1w * (2 * phi_z - Consts::factor2w * phi_z_last +
            m_z * 2 * Consts::dt * Consts::dt /
                (Particle::particleMass * this->selfR * this->selfR));


    // Update angles for bonds:
    // Particles rotate thus we should recalculate angles of springs direction:
    int len = bonds_angles.size();
    for (int i = 0; i < len; i++)
        bonds_angles[i] += phi_z - phi_z_help;

    // Update velocities:
    v_x = (x - x_last) / (2 * Consts::dt);
    v_y = (y - y_last) / (2 * Consts::dt);
    v_z = (z - z_last) / (2 * Consts::dt);
    w_z = (phi_z - phi_z_last) / (2 * Consts::dt);

    // Save previous data:
    x_last = x_help;
    y_last = y_help;
    z_last = z_help;
    phi_z_last = phi_z_help;

    f_x_last = f_x;
    f_y_last = f_y;
    f_x = f_y = f_z = m_z = 0;
}

void MovingMorseParticle::updateActivation() {
    if (time_activation > 0){
        ext_activation = Particle::levelMinActiv / (pow(tau_act / time_activation, 2) + 1);
        time_activation += 1;
    }
}

void MovingMorseParticle::doPrint(std::ostream& os) const {
    // output all data which is needed to load, e.g. forces will be recalculated
    // so no need to store them
    os << "ident:" << ident << "\n"
        << "x:" << x << "\n"
        << "y:" << y << "\n"
        << "z:" << z << "\n"
        << "x_last:" << x_last << "\n"
        << "y_last:" << y_last << "\n"
        << "z_last:" << z_last << "\n"
        << "phi_z:" << phi_z << "\n"
        << "phi_z_last:" << phi_z_last << "\n"
        << "v_x:" << v_x << "\n"
        << "v_y:" << v_y << "\n"
        << "v_z:" << v_z << "\n"
        << "w_z:" << w_z << "\n";
    os << "bonds:";
    for(size_t i = 0; i < bonds.size(); ++i) {
        os << bonds[i];
        if( i != bonds.size() - 1 )
            os << ',';
    }
    os << "\n";
    os << "bonds_angles:";
    for(size_t i = 0; i < bonds_angles.size(); ++i) {
        os << bonds_angles[i];
        if( i != bonds.size() - 1 )
            os << ',';
    }
    os << "\n";
    os << "ext_activation:" << ext_activation << "\n"
        << "time_activation:" << time_activation << "\n"
        << "selfR:" << selfR << "\n"
        << "state:" << state % NUM_STATES << "\n"
        << "onceAttached:" << onceAttached << "\n";
    os << "end\n";
}

void MovingMorseParticle::loadParticle(std::ifstream& dump) {
    std::string line;
    while(std::getline(dump, line), line != "end") {
        size_t delim = line.find(':');
        std::string key = line.substr(0, delim);
        std::string value = line.substr(delim + 1, line.size() - delim - 1);
        if( key == "ident") {
            ident = std::stoll(value);
        } else if( key == "x") {
            x = std::stod(value);
        } else if( key == "y") {
            y = std::stod(value);
        } else if( key == "z") {
            z = std::stod(value);
        } else if( key == "x_last") {
            x_last = std::stod(value);
        } else if( key == "y_last") {
            y_last = std::stod(value);
        } else if( key == "z_last") {
            z_last = std::stod(value);
        } else if( key == "phi_z") {
            phi_z = std::stod(value);
        } else if( key == "phi_z_last") {
            phi_z_last = std::stod(value);
        } else if( key == "v_x") {
            v_x = std::stod(value);
        } else if( key == "v_y") {
            v_y = std::stod(value);
        } else if( key == "v_z") {
            v_z = std::stod(value);
        } else if( key == "w_z") {
            w_z = std::stod(value);
        } else if( key == "ext_activation") {
            ext_activation = std::stod(value);
        } else if( key == "time_activation") {
            time_activation = std::stod(value);
        } else if( key == "selfR") {
            selfR = std::stod(value);
        } else if( key == "state") {
            state = (PlateletState)std::stoi(value);
        } else if( key == "onceAttached") {
            onceAttached = (bool)std::stoi(value);
        } else if( key == "bonds") {
            size_t comma;
            while( comma = value.find(','), comma != std::string::npos ) {
                bonds.push_back(std::stoi(value.substr(0, comma)));
                value = value.substr(comma + 1, value.size() - comma - 1);
            }
            if( value.size() > 0 ) {  // there is no comma after last number
                bonds.push_back(std::stoi(value));
            }
        } else if( key == "bonds_angles") {
            size_t comma;
            while( comma = value.find(','), comma != std::string::npos ) {
                bonds_angles.push_back(std::stod(value.substr(0, comma)));
                value = value.substr(comma + 1, value.size() - comma - 1);
            }
            if( value.size() > 0 ) {  // there is no comma after last number
                bonds_angles.push_back(std::stod(value));
            }
        } else {
            std::cerr << "Unknown field " << key << " with value " << value
                << " for particle of type anchored\n";
        }
    }
}
