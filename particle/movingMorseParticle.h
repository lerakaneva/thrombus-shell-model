#pragma once 


#include <random>

#include "particle.h"
#include "../hydrodynamics/hydrodynamics.h"


struct MovingMorseParticle final : Particle {
    // Constants for pl-pl interaction via integrins (Morse).
    static double const alpha; // Dependence on distance.
    static double const k_morse; // Range of force.
    // Constants for pl-pl interaction via GPIb (stochastic springs).
        // Association:
    static double const k_as_0; // Maximum rate of bond creation.
            // Probability of bond creation dependence
            //on distance between platelets:
    static double const alpha_as;
        // General
    static double const eq_spring_l;  // 1/2 of equilibrium spring length.
    static double const k_stochastic;  // Coefficient of restitution.
        // Dissociation:
    static double const k_dis_0; // Maximum rate of bond dissociation.
            // Probability of bond creation dependence
            //on force between platelets:
    static double const bell_dis;
        // Elastic interaction:
    static double const k_wall;  // Repulsion from wall.
    static double const k_repulsion;  // Repulsion between platelets.

    // random generation related objects for radius
    static std::default_random_engine gen;
    static std::normal_distribution<double> dist;

    MovingMorseParticle() {} // for manual setting fields
    MovingMorseParticle(T x, T y, T z, unsigned int ident,
        float ext_activation, int time_activation, PlateletState state=isFree);
    MovingMorseParticle(std::array<double, 3> coords, Hydrodynamics& flow, unsigned int ident);

    void interact_with(Particle& another);

    void interact_with_walls();

    virtual void updateForces(T f_x, T f_y, T f_z, T m_z);

    virtual void interact_with(Hydrodynamics& flow);

    virtual void updateDynamics();

    virtual void updateActivation();

    virtual void doPrint(std::ostream& os) const;
    virtual void loadParticle(std::ifstream&);
};