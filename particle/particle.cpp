#include "particle.h"

void Particle::create_bond(Particle& another) {
    T l = this->distance(another);
    // Determine initial dirac
    if (l == 0) {
        std::cout << ":/ bond creation \n";
    }
    T angle_this = acos((another.x - this->x) / l);
    T angle_another = acos((this->x - another.x) / l);
    if (another.y - this->y < 0)
        angle_this *= -1;
    if (this->y - another.y < 0)
        angle_another *= -1;
    this->bonds.push_back(another.ident);
    this->bonds_angles.push_back(angle_this);
    another.bonds.push_back(this->ident);
    another.bonds_angles.push_back(angle_another);
}

double Particle::return_angle(Particle& another) {
    std::vector<unsigned int>::iterator search_result;
    search_result = find (this->bonds.begin(),
                          this->bonds.end(), another.ident);
    // Return correspondent angle:
    return *(this->bonds_angles.begin() + std::distance(this->bonds.begin(),
        search_result));
}

void Particle::kill_bond(Particle& another) {
    this->kill_bond_one_side(another);
    another.kill_bond_one_side(*this);
}
    
void Particle::kill_bond_one_side(Particle& another) {
    std::vector<unsigned int>::iterator to_delete_another;
    to_delete_another = find (another.bonds.begin(),
                              another.bonds.end(), this->ident);
    if (to_delete_another != another.bonds.end()){
        // Delete correspondent angle:
        another.bonds_angles.erase(another.bonds_angles.begin() +
                std::distance(another.bonds.begin(), to_delete_another));
        another.bonds.erase(to_delete_another);
    }
}