set(SOURCE_FILES 
        particle.cpp
        AnchoredParticle.cpp
        movingMorseParticle.cpp
)

add_library(Particle ${SOURCE_FILES})
target_link_libraries(Particle)