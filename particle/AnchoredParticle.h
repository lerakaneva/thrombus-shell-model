#pragma once

#include "particle.h"

struct AnchoredParticle final : Particle {
    AnchoredParticle() {}
    AnchoredParticle(T x, T y, T z, T x_last, T y_last, T z_last,
                      T phi_z, T phi_z_last, unsigned int ident,
                      float ext_activation, int time_activation, PlateletState state=isSeed) :
        Particle(x, y, z, x_last, y_last, z_last, phi_z, phi_z_last, ident, ext_activation, time_activation, state) {
            bonds.reserve(20);
            bonds_angles.reserve(20);
        }

    virtual void updateForces(T, T, T, T) {}

    virtual void interact_with(Hydrodynamics&) {}

    virtual void updateDynamics() {}

    virtual void updateActivation() {} 

    void doPrint(std::ostream&) const;
    virtual void loadParticle(std::ifstream&) final;
};