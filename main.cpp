#include <time.h>  // for time()
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <array>

#include <boost/program_options.hpp>
namespace po=boost::program_options;
#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

// simple hydro setup
#include "hydrodynamics/SimpleHydrodynamics.h"
typedef SimpleHydrodynamics Hydro;
#include "geometry/SimpleThrombosisGeometry.h"
typedef SimpleThrombosisGeometry Geo;


// particlesContainer
#include "main.h"
#include "particle/particle.h"
#include "particle/AnchoredParticle.h"
#include "particle/movingMorseParticle.h"
typedef MovingMorseParticle MP;
#include "particlesContainer.hpp"
#include "constants.h"

typedef double T;
using std::vector;
using std::cout;
using std::ofstream;

uint64_t Random::seed;  // declaration of static member to avoid linker errors

int main(int argc, char ** argv) {
    std::ios_base::sync_with_stdio(false);
    unsigned int seed = time(NULL);
    Random::set_seed(seed);
    std::cout << "random seed: " << seed << "\n";
    double max_time = 15000; // time in miliseconds

    po::options_description desc("Program options");
    bool _continue_flag = false;  // label whether we start new calculation or load results and continue previous one
    desc.add_options()
        ("help,h", "Show help and exit")
        ("continue", po::bool_switch(&_continue_flag)->default_value(false),
            "Provide this flag to continue from existing data")
        ("end_time,t", po::value<double>(&max_time),
            "Final time in miliseconds")
    ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    if (vm.count("help")) {
        std::cout << "Usage: " << argv[0] << " [options]\n";
        std::cout << desc;
        return 0;
    }
    if( vm.count("continue") ? vm["continue"].as<bool>() : false ) {
        _continue_flag = true;
    }
    if( vm.count("end_time") ) {
        std::cout << "max_time set to " << max_time << "\n";
    }

    // create directory for various data
	if( !fs::exists("data") )
		fs::create_directory("data");
	if( !fs::exists("data/dumps") )
		fs::create_directory("data/dumps");

    unsigned long long max_iter = max_time / Consts::dt;  // Length of time cycle.

    const double writeInterval = 20; // frequency of writing in miliseconds

    Hydro flow;
    Geo geo;

    // Initiating system of particles:
    ParticlesContainer<MP, AnchoredParticle, Hydro, Geo>
                partSystem(flow, geo, !_continue_flag);
    partSystem.writeCSVFreq = writeInterval / Consts::dt;
    partSystem.writeStateFreq = writeInterval * 100 / Consts::dt;
    // Initiating flow in vessel
    flow.initialize(partSystem.particles());

    unsigned long long i = _continue_flag ?
        (partSystem.getInitialStep() + 1) * partSystem.writeCSVFreq : 0;
    for(; i <= max_iter; i++) {
        if (i % partSystem.writeCSVFreq == 0) {
            partSystem.writeCSV(i / partSystem.writeCSVFreq);  // Output.
            if( i % partSystem.writeStateFreq == 0 ) {
                partSystem.dumpState(i / partSystem.writeCSVFreq); // dump state every 5 seconds
                partSystem.flushAll();
            }
            // Showing progress:
            cout << "\rComplete " << (unsigned int) (i * 1. / max_iter * 100)
                << "\% (" << i * Consts::dt << " miliseconds) now there are " << partSystem.size() << " particles";
            cout.flush();
        }
        ++partSystem; // Calculation of next step of platelets dynamics.
        flow.step(i, partSystem.particles());  // Check geometry and update flow if needed
    }
    cout << "\n";
    return 0;
}
