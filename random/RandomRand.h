#pragma once

//#include "Random.h"

class RandomRand {
    static uint64_t seed;
public:
    void static set_seed(uint64_t val) {seed = val; srand(val);}

    static double urand_num(double from, double to) {
        return from + rand() * 1. / RAND_MAX * (to - from);
    }

    static double big_urand_num(double from, double to) {
        return from + rand() * 1. / RAND_MAX * (to - from);
    }
};