#pragma once

// for now this file is useless, because C++ doesn't have static polymorphysm
// We should either create instance of Random class and use non-static functions
// or think of some workaround, like template specialization.
// For now just create new random classes with the interface below:

class Random {
    static uint64_t seed;

public:
    virtual void static set_seed(uint64_t val) = 0;

    virtual static double urand_num(double from, double to) = 0;

    virtual static double big_urand_num(double from, double to) = 0;
};
