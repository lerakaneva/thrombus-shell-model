from constants import Constants

class Parser:
    x = []
    y = []
    times = []
    colors = []
    states = []
    radii = []
    fx = []
    fy = []
    
    def parse_coord_file(self, filename):
        with open(filename) as f:
            block_idx = -1
            for i, line in enumerate(f):
                line = line.strip()
                if line[0] == "*":
                    block_idx += 1
                    self.x.append([])
                    self.y.append([])
                    self.times.append(float(line.split(' ')[-1]) * Constants.writeCSV * Constants.dt)
                else:
                    try:
                        xi, yi, _ = map(float, line.split(' '))
                    except:
                        self.x = self.x[:-1]
                        self.y = self.y[:-1]
                        break
                    if xi == 0 and yi == 0:
                        continue
                    self.x[block_idx].append(xi)
                    self.y[block_idx].append(yi)

    def parse_colormap_file(self, filename):
        with open(filename) as f:
            for i, line in enumerate(f):
                idx, time, r, g, b = map(int, line.strip().split(' '))
                rel_time = time - int(self.times[0] / Constants.writeCSV / Constants.dt)
                if rel_time + 1 > len(self.colors):
                    self.colors.append([])
                if idx < len(self.x[rel_time]):
                    self.colors[rel_time].append("#{:02x}{:02x}{:02x}".format(r, g, b))
                
    def parse_forces_file(self, filename):
        with open(filename) as f:
            for i, row in enumerate(f):#data):
                pairs = row.strip().split('  ')
                self.fx.append([])
                self.fy.append([])
                for j, pair in enumerate(pairs):
                    vals = pair.split(' ')
                    if len(vals) == 3:  # for compatibility with previos output format, may be removed later
                        ui, vi, ci = map(float, vals)
                    elif len(vals) == 2:
                        ui, vi = map(float, vals)
                    else:
                        break
                    self.fx[i].append(ui)
                    self.fy[i].append(vi)

    
    def parse_props_file(self, filename):
        with open(filename) as f:
            for i, row in enumerate(f):
                pairs = row.strip().split('  ')
                self.states.append([])
                self.radii.append([])
                for pair in pairs:
                    vals = pair.split(' ')
                    self.states[i].append(int(vals[0]))
                    self.radii[i].append(float(vals[1]))
    
    def parse_partsystem(self, filename):
        pass
