#!/usr/bin/python3

import sys
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import xml.etree.ElementTree
import csv

from constants import Constants
from Parser import Parser

colors = {0: 'grey', 1: 'red', 3: 'yellow', 4: 'green'}
states = {0: 'free', 1: 'anchored', 3: 'attached', 4: 'dummy anchored'}

parser = argparse.ArgumentParser(description='Script for visualuizing model results')
parser.add_argument("-d", "--directory", default=".", help="directory, where files are stored")
parser.add_argument("--save", action='store_true')
parser.add_argument("--dt", default=0.0001, help="Time step")
parser.add_argument("--time", help="Final time, ms, defaults to maximun available from files")
parser.add_argument("--forces", action='store_true')
parser.add_argument("--states", action='store_true', help='show states instead of activations')
parser.add_argument("-b", help="L / L0...")
parser.set_defaults(save=False, b=0)

args = parser.parse_args()

folder = args.directory
Constants.dt = float(args.dt)
Constants.b = float(args.b)

frame = []
if os.path.exists(os.path.join(folder, 'desc_PVis.xml')):
    e = xml.etree.ElementTree.parse(os.path.join(folder, 'desc_PVis.xml')).getroot()
    static_obj = e.find('static')
    for line in static_obj.findall('line'):
        c1 = line.find('corner1')
        c2 = line.find('corner2')
        frame.append(((float(c1.find('x').text), float(c2.find('x').text)), (float(c1.find('y').text), float(c2.find('y').text))))
else:
    print("Cannot find file" + os.path.join(folder, 'desc_PVis.xml') + ", cannot draw border")

file_parser = Parser()

file_parser.parse_coord_file(os.path.join(folder, "coordinates_ParticleVis.txt"))
x = file_parser.x
y = file_parser.y
times = file_parser.times

file_parser.parse_props_file(os.path.join(folder, "props.txt"))
if args.states:
    c = file_parser.states
else:
    file_parser.parse_colormap_file(os.path.join(folder, "colormap_ParticleVis.txt"))
    c = file_parser.colors

R = file_parser.radii

def getR(t, i):
    if R:
        return R[t][i]
    else:
        return 1

'''
# to show not every frame
x = x[::Constants.step]
y = y[::Constants.step]
c = c[::Constants.step]
u = u[::Constants.step]
v = v[::Constants.step]
'''

final_T = int(args.time) if args.time else times[-1]
    
nframes = len(x)
if args.time:
    for i, t in enumerate(times):
        if t > final_T:
            nframes = i
            break
print("nfames: ", nframes)

fig, ax = plt.subplots()

for line in frame:
    ax.add_line(mlines.Line2D(*line))

if args.states:
    handles = []
    for idx in states:
        #handles.append(plt.Circle((0, 0), 1, color=colors[idx], label=states[idx]))
        handles.append(mlines.Line2D([0], [0], label=states[idx], markerfacecolor=colors[idx]
                                     , color='white', marker='o', markeredgecolor='black', markersize=10))
    ax.legend(title='Particle types', handles=handles, loc=1)

circles = []
for i in range(len(x[0])):
    circles.append(plt.Circle((x[0][i], y[0][i]), getR(0, i)
                              , color=(c[0][i] if not args.states else colors[c[0][i]]), ec='black'))
    ax.add_artist(circles[-1])


ax.set_xlim(min(min(xx[0]) for xx in frame), max(max(xx[0]) for xx in frame))
ax.set_ylim(min(min(xx[1]) for xx in frame), max(max(xx[1]) for xx in frame))
ax.set_aspect('equal', adjustable='box')
text = ax.text(0.1, 1.02, '', transform=ax.transAxes)

from time import time
draw_times = []
def animate(i):
    '''Function for updating animation'''
    start_time = time()
    global circles, flow_idx
    while len(circles) > len(x[i]):
        circles[-1].remove()
        circles.pop()
    for j in range(len(x[i])):
        col = c[i][j] if not args.states else colors[c[i][j]]
        rad = getR(i, j)
        if j > len(circles) - 1:
            circles.append(plt.Circle((x[i][j], y[i][j]), rad, color=col, ec='black'))
            ax.add_artist(circles[-1])
        else:
            circles[j].center = x[i][j], y[i][j]
            circles[j].set_radius(rad)
            circles[j].set_color(col)
            circles[j].set_ec('black')
    text.set_text("time {0:.0f} ms, N = {1}".format(times[i], len(x[i])))

    frame_time = time() - start_time
    draw_times.append(frame_time)
#    print("Frame drawn in {:.5f} sec".format(frame_time))
    return fig

ani_kwargs = {'fig': fig, 'func': animate, 'frames': range(0, nframes)
              , 'save_count': nframes, 'interval': 59, 'repeat': False}

fig.set_size_inches(16, 7)
#fig.tight_layout()
if parser.parse_args().save:
    from matplotlib.animation import FuncAnimation
    from time import time
    start = time()
    ani = FuncAnimation(**ani_kwargs)
    # this command generates h264-coded video file - high quality, low size
    #ani.save(folder + 'cartoon.mp4', writer='ffmpeg',  fps=30,
          #extra_args=['-vcodec', 'h264', '-pix_fmt', 'yuv420p'])
    # this command generates video in format that can be played on most machines without problems
    ani.save(os.path.join(folder, 'cartoon.avi'), writer='ffmpeg',  fps=30,
          extra_args=['-vcodec', 'msmpeg4v2', '-pix_fmt', 'yuv420p', '-qscale:v', '4'])
    print("Saving took {:.2f} seconds".format(time() - start))
else:
    from Anim_Player import Player
    ani = Player(maxi=nframes-1, **ani_kwargs)
    plt.show()
print("Average drawing time: {:.5f} sec".format(sum(draw_times) / len(draw_times)))
